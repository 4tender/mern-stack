import { createStore, applyMiddleware, compose } from "redux";
import rootReducer from "../reducers";
import createSagaMiddleware from "redux-saga";
import apiSaga from "../sagas/api-saga";

const SagaMiddleware = createSagaMiddleware();

const reduxDevTools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();
const store = createStore(rootReducer,
        compose(applyMiddleware(SagaMiddleware),
        reduxDevTools)
);

SagaMiddleware.run(apiSaga);

export default store;