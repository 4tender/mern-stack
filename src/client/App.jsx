import React, { Fragment } from 'react';
import Header from './components/Header';
import 'normalize.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.sass'
import { Route, BrowserRouter as Router } from "react-router-dom";

const App = () => (
  <Router>
    <Fragment>
      <Header/>
    </Fragment>
  </Router>
)

export default App;