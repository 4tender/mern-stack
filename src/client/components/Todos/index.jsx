import React, { Component } from 'react'
import {Input, Container, Row, Col, ListGroup, ListGroupItem } from 'reactstrap'
import './Container.sass'

export default class TodosContainer extends Component {
  render() {
    return (
      <Container className="todo-wrapper">
        <Row>
            <Col md={{ size: 6, offset: 3 }}>
                <Input placeholder="Input todo to search"/>
                <ListGroup flush>
                    <ListGroupItem disabled tag="button">Cras justo odio</ListGroupItem>
                    <ListGroupItem tag="button">Dapibus ac facilisis in</ListGroupItem>
                    <ListGroupItem tag="button">Morbi leo risus</ListGroupItem>
                    <ListGroupItem tag="button">Porta ac consectetur ac</ListGroupItem>
                    <ListGroupItem tag="button">Vestibulum at eros</ListGroupItem>
                </ListGroup>
            </Col>
        </Row>
      </Container>
    )
  }
}
