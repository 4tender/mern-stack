import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import IconButton from '@material-ui/core/IconButton';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Typography from '@material-ui/core/Typography';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import styles from './menuStyles';
import { withStyles } from '@material-ui/core/styles';

const Menu = (props) => {
    const { classes } = props;
    const { isMenuOpen } = props;

    return (<Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={ isMenuOpen }
        classes={{
            paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <Typography variant="h5" color="inherit" noWrap>
              Меню
          </Typography>
          <IconButton onClick={props.onMenuClick}>
            <ChevronLeftIcon/>
          </IconButton>
        </div>
        <List>
          {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
            <ListItem button key={text}>
              <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>
      </Drawer>)
}

export default withStyles(styles, { withTheme: true })( Menu )