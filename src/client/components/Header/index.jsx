import React, { Component, Fragment } from 'react'
import Bar from './Bar';
import Menu from './Menu';



class Header extends Component {

  state = {
    isMenuOpen: false,
  };

  handleDrawer = () => {
    this.setState({ isMenuOpen: !this.state.isMenuOpen });
  };

  render() {
    return (
      <Fragment>
        <Bar isMenuOpen={this.state.isMenuOpen} onMenuClick={this.handleDrawer} />
        <Menu isMenuOpen={this.state.isMenuOpen} onMenuClick={this.handleDrawer} />
      </Fragment>
    )
  }
}


export default Header;