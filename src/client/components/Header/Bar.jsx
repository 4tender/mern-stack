import React from 'react';
import classNames from 'classnames';
import styles from './barStyles';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import IconButton from '@material-ui/core/IconButton';
import { withStyles } from '@material-ui/core/styles';

const Bar = (props) => {
    const { classes } = props;
    const { isMenuOpen } = props;

    return (
        <AppBar
        position="fixed"
        className={classNames(classes.appBar, {
          [classes.appBarShift]: isMenuOpen,
        })}
      >
        <Toolbar disableGutters={!isMenuOpen}>
          <IconButton
            color="inherit"
            aria-label="Open drawer"
            onClick={props.onMenuClick}
            className={classNames(classes.menuButton, isMenuOpen && classes.hide)}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" color="inherit" noWrap>
            Persistent drawer
          </Typography>
        </Toolbar>
      </AppBar>
    )
}

export default withStyles(styles, { withTheme: true })(Bar);