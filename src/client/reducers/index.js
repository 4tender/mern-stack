import { ADD_TASK } from '../constants/actions-types/tasks'

const initialState = [
    
];

function rootReducer(state = initialState, action) {
    console.log(action)
    switch (action.type) {
        case ADD_TASK:
            return [...state, action.payload]
        default:
            return state;
    }
};

export default rootReducer;