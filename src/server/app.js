import express from 'express';
import { MongoClient } from 'mongodb';
import bodyParser from 'body-parser';
import path from 'path';
import routes from './routes'

const app = express();
const port = 8000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.resolve(__dirname, '..', '..', 'public')));
app.use('/', routes);

app.listen(port, () => {
  console.log('We are live on ' + port);
});