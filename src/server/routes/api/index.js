import { Router } from 'express';
import fs from 'fs';


const router = Router();

fs.readdirSync(__dirname).forEach((file) => {
    const viewName = file.substr(0, file.indexOf('.'))
    if(file != 'index.js')
        require(`./${viewName}`)(router,viewName);
    else
        return;
});

export default router;