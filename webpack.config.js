const HtmlWebPackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const path = require('path');

const outputDir = path.resolve(__dirname, 'public');
const entryDir = path.resolve(__dirname, 'src', 'client');

const entryFile = path.resolve(entryDir, 'index.js');
const entryTemplate = path.resolve(entryDir, 'index.html');
const outputHtml = path.resolve(outputDir, 'index.html');

module.exports = (env, argv) => ({
 entry: ['@babel/polyfill', entryFile],
 output: {
      filename: 'bundle.[hash].js',
      path: outputDir
 },
 module: {
      rules: [
        {
            test: /\.html$/,
            use: [
              {
                loader: "html-loader"
              }
            ]
        },
        {
          test: /\.(js|jsx)$/,
          loader: 'babel-loader',
          exclude: /node_modules/
        },
        {
        test: /\.(sass|css)$/,
        use: [
          'style-loader',
          'css-loader',
            {
              loader: "postcss-loader",
              options: {
                ident: "postcss",
                sourceMap: true,
                plugins: () => argv.mode === 'production' ? [
                  require("cssnano")({
                    preset: [
                      "default",
                      {
                        discardComments: {
                          removeAll: true
                        }
                      }
                    ]
                  })
                ] : []
              }
            },
          'sass-loader' 
        ]
    },
    {
      test: /\.(eot|svg|ttf|woff|woff2|ttc)$/,
      use: [
               {
                   loader: 'file-loader?name=./static/fonts/[name].[ext]'
               }
           ]
    }
      ]
    },
    resolve: {
      extensions: ['.js', '.jsx'],
    },
    devServer: {
      host: '0.0.0.0',//your ip address
      port: 8080,
      contentBase: [
        path.resolve(outputDir, "static"),
        path.resolve(entryDir, "assets")
      ],
      compress: true,
    },
    plugins: [
      new CleanWebpackPlugin(['public']),
      new HtmlWebPackPlugin({
        template: entryTemplate,
        filename: outputHtml
      })
    ]
});